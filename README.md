# Front End Project for R2-Admin
> Featuring Material Design 2, Webpack 2, HMR (Hot Module Replacement), and @ngrx.

```
npm install
npm start
```

## Features

* Angular 2
  * Async loading
  * Treeshaking
* Webpack 2
* HMR (Hot Module Replacement)
* TypeScript 2
  * @types
* Material Design 2
* @ngrx
  * store (RxJS powered state management for Angular2 apps, inspired by Redux)
  * effects (Side effect model for @ngrx/store)
  * router-store (Bindings to connect angular/router to ngrx/store)
  * store-devtools (Developer Tools for @ngrx/store)
  * store-log-monitor (Log Monitor for @ngrx/store-devtools and Angular 2)
  * ngrx-store-logger (Advanced console logging for @ngrx/store applications, ported from redux-logger.)
  * ngrx-store-freeze in dev mode (@ngrx/store meta reducer that prevents state from being mutated.)
* Karma/Jasmine testing
* Protractor for E2E testing

## Basic scripts

Use `npm start` for dev server. Default dev port is `4200`.

Use `npm run start:hmr` to run dev server in HMR mode.

Use `npm run build` for production build.

Use `npm run server:dist` opens the dist folder in the browser.

Use `npm test` runs karma tests.

Use `npm run server:coverage` opens the test coverage report in the browser.

use `npm run e2e -s` runs the e2e tests in PhantomJS.

use `npm run docs` CURRENTLY NOT SUPPORTED FOR TYPESCRIPT 2.0 (Pending update).

### Store Log Monitor / Store Logger

### HMR (Hot Module Replacement)

HMR mode allows you to update a particular module without reloading the entire application.
The current state of your app is also stored in @ngrx/store allowing you to make updates to your
code without losing your currently stored state.