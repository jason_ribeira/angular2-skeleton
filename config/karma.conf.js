module.exports = function(config) {
  var testWebpackConfig = require('./webpack.test.js');

  var configuration = {

    basePath: '',
    frameworks: ['jasmine'],

    exclude: [ ],
    files: [ { pattern: './spec-bundle.js', watched: false } ],
    preprocessors: { './spec-bundle.js': ['coverage', 'webpack', 'sourcemap'] },

    webpack: testWebpackConfig,

    coverageReporter: {
      type: 'in-memory'
    },

    remapCoverageReporter: {
      'text-summary': null,
      json: './coverage/coverage.json',
      html: './coverage/html'
    },

    webpackServer: { noInfo: true },
    reporters: [ 'mocha', 'coverage', 'remap-coverage' ],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: [
      'PhantomJS'
    ],

    singleRun: true
  };

  config.set(configuration);
};