const webpack = require('webpack');
const helpers = require('./helpers');

const ProvidePlugin = require('webpack/lib/ProvidePlugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const NamedModulesPlugin = require('webpack/lib/NamedModulesPlugin');

module.exports = {

  entry: {},

  devtool: 'inline-source-map',

  resolve: {

    extensions: ['.ts', '.js']

  },

  module: {

    rules: [

      {
        test: /\.ts$/,
        enforce: 'pre',
        loader: 'tslint-loader',
        exclude: [helpers.root('node_modules')]
      },

      {
        test: /\.js$/,
        enforce: 'pre',
        loader: 'source-map-loader',
        exclude: [
          helpers.root('node_modules/@angular'),
          helpers.root('node_modules/rxjs')
        ]
      },

      {
        test: /\.ts$/,
        loaders: [
          'awesome-typescript-loader?sourceMap=false,inlineSourceMap=true,compilerOptions{}=removeComments:true'
        ],
        exclude: [/\.e2e\.ts$/]
      },

      {test: /\.json$/, loader: 'json-loader', exclude: [helpers.root('src/index.html')]},

      {test: /\.css$/, loaders: ['to-string-loader', 'css-loader'], exclude: [helpers.root('src/index.html')]},

      {test: /\.html$/, loader: 'raw-loader', exclude: [helpers.root('src/index.html')]},

      {
        test: /\.(js|ts)$/, loader: 'istanbul-instrumenter-loader',
        enforce: 'post',
        include: helpers.root('src'),
        exclude: [
          /\.(e2e|spec)\.ts$/,
          /node_modules/
        ]
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loaders: ['raw-loader', 'sass-loader'] // sass-loader not scss-loader
      }
    ]
  },
  plugins: [
    new webpack.ContextReplacementPlugin(
      /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
      helpers.root('./src')
    ),
    new DefinePlugin({
      AOT: false,
      ENV: JSON.stringify('test'),
      HMR: false,
      PORT: 3000,
      HOST: JSON.stringify('localhost'),
      UNIVERSAL: false
    }),
    new NamedModulesPlugin(),
    new webpack.LoaderOptionsPlugin({
      options: {
        tslint: {
          emitErrors: false,
          failOnHint: false,
          resourcePath: helpers.root('src')
        }
      }
    })
  ],

  node: {
    global: true,
    process: false,
    crypto: false,
    module: false,
    clearImmediate: false,
    setImmediate: false
  }
};