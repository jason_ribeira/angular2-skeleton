const webpack = require('webpack');
const helpers = require('./helpers');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const CheckerPlugin  = require('awesome-typescript-loader').CheckerPlugin;
const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = function (options) {
  isProd = options.env === 'production';
  return {
    entry: {

      'polyfills': './src/polyfills.ts',
      'vendor': './src/vendor.ts',
      'main': './src/main.ts'

    },
    resolve: {
      extensions: ['.ts', '.js', '.json']
    },

    module: {
      rules: [
        {
          test: /\.js$/,
          loader: 'source-map-loader',
          exclude: [
            helpers.root('node_modules/@angular'),
            helpers.root('node_modules/rxjs')
          ]
        },
        {
          test: /\.ts$/,
          loaders: [
            '@angularclass/hmr-loader?pretty=' + !isProd + '&prod=' + isProd,
            'awesome-typescript-loader'
          ],
          exclude: [/\.(spec|e2e)\.ts$/]
        },
        {test: /\.json$/, loader: 'json-loader'},
        {test: /\.html/, loader: 'raw-loader', exclude: [helpers.root('src/index.html')]},
        {test: /\.css$/, loader: 'raw-loader'},
        {
          test: /\.scss$/,
          exclude: /node_modules/,
          loaders: ['raw-loader', 'sass-loader'] // sass-loader not scss-loader
        }
      ]
    },

    plugins: [
      new ContextReplacementPlugin(
        /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
        helpers.root('src')
      ),
      new webpack.ProgressPlugin(),
      new CheckerPlugin (),
      new webpack.NamedModulesPlugin(),
      new webpack.optimize.CommonsChunkPlugin({
        name: ['polyfills', 'vendor'].reverse()
      }),
      new CopyWebpackPlugin([{
        from: 'src/assets',
        to: 'assets'
      }]),
      new HtmlWebpackPlugin({
        template: 'src/index.html',
        chunksSortMode: 'dependency'
      })
    ],

    node: {
      global: true,
      process: true,
      Buffer: false,
      crypto: true,
      module: false,
      clearImmediate: false,
      setImmediate: false,
      clearTimeout: true,
      setTimeout: true
    }

  };
}