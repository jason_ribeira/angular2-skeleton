import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from "rxjs";

@Component({
    selector: 'main-route',
    template: require('./dashboard.component.html'),
    styles: [ require('./dashboard.component.scss') ]
})
export class DashboardComponent implements OnInit {

    counter : number;
    
    sub : any;
    
    constructor(private _store : Store<any>) {
        
    }

    ngOnInit() {
    }

}