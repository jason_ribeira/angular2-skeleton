import { NgModule } from '@angular/core';
import { DashboardComponent } from "./dashboard.component";
import { dashboardRouting } from "./dashboard.routing";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
    imports: [ dashboardRouting, SharedModule ],
    declarations: [ DashboardComponent ],
    exports: []
})
export class DashboardModule {
}