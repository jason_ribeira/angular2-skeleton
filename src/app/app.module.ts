import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';

import { DashboardModule } from "./routes";
import { baseRouting, appRoutingProviders } from "./app.routing";

import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';
import { SharedModule } from "./shared/shared.module";
import { HeaderComponent, SidebarComponent, SidebarSubMenuDirective } from "./shared/components";

import { StoreModule, Store } from '@ngrx/store';

import { storeProviders, AppState, GlobalActions, rootReducer } from "./shared/store";

import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreLogMonitorModule, useLogMonitor } from '@ngrx/store-log-monitor';
import {MaterialModule} from "@angular/material";

const routeModules = [
    DashboardModule
];

const ngrxModules = [
    StoreModule.provideStore(rootReducer),
    StoreLogMonitorModule
];

if (ENV === 'development') {
    ngrxModules.push(
        StoreDevtoolsModule.instrumentStore({
            monitor: useLogMonitor({
                visible: false,
                position: 'right'
            })
        })
    )
}

@NgModule({
    imports: [
        BrowserModule,
        baseRouting,
        MaterialModule.forRoot(),
        SharedModule,
        ...routeModules,
        ...ngrxModules
    
    ],
    declarations: [ AppComponent, HeaderComponent, SidebarComponent, SidebarSubMenuDirective ],
    bootstrap: [ AppComponent ],
    providers: [ appRoutingProviders, storeProviders ]
})
export class AppModule {
    
    constructor(private appRef : ApplicationRef, private _store : Store<AppState>) {
    }
    
    hmrOnInit(store) {
        if (!store || !store.rootState) return;
        
        if (store.rootState) {
            this._store.dispatch(GlobalActions.setRootState(store.rootState))
        }
        
        if ('restoreInputValues' in store) {
            store.restoreInputValues();
        }
        this.appRef.tick();
        delete store.state;
        delete store.restoreInputValues;
    }
    
    hmrOnDestroy(store) {
        var cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
        this._store.select(s=>s).subscribe(s => store.rootState = s);
        store.disposeOldHosts = createNewHosts(cmpLocation);
        store.restoreInputValues = createInputTransfer();
        removeNgStyles();
    }
    
    hmrAfterDestroy(store) {
        store.disposeOldHosts();
        delete store.disposeOldHosts;
        // anything you need done the component is removed
    }
    
}
