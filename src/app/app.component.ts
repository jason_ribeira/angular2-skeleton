import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { Store } from "@ngrx/store";
import { GlobalActions } from "./shared/store/global/global.actions";
import { AppState, getSidebarState } from "./shared/store/index";
import { Subscription } from "rxjs";

@Component({
    selector: 'my-app',
    template: require('./app.component.html'),
    styles: [require('./app.component.scss')],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit, OnDestroy {
    
    isDev : boolean = false;
    
    private sbClosedSize : number = 50;
    private sbOpenedSize : number = 200;
    
    sbOpened : boolean = true;
    
    _sidebarToggleSubscription : Subscription;
    
    constructor(private _store : Store<AppState>) {
    }

    ngOnInit() {
        if (ENV === 'development') this.isDev = true;
        
        this._sidebarToggleSubscription = this._store.let(getSidebarState())
            .subscribe(s => {
                this.sbOpened = s;
                
            })
        
    }
    
    ngOnDestroy() {
        this._sidebarToggleSubscription.unsubscribe();
    }
    
    toggleSidebar(e) {
        console.log('emission heard');
        this._store.dispatch(GlobalActions.toggleSidebar());
    }
    
    getDynamicSize() {
        return this.sbOpened ? this.sbOpenedSize + 'px' : this.sbClosedSize + 'px';
    }
    
}