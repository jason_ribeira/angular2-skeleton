import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import { ActionReducer, Action } from '@ngrx/store';

import { GlobalActions } from './global.actions';

export const setRootState = (reducer : ActionReducer<any>) : ActionReducer<any> => {
    return function (state, action) {
        
        switch (action.type) {
            case GlobalActions.SET_ROOT_STATE:
                return action.payload;
            default:
                return reducer(state, action);
        }
        
    };
};

const initialSidebarState : boolean = true;

export const toggleSidebar = (state : boolean = initialSidebarState, action : Action) : boolean => {
    switch (action.type) {
        case GlobalActions.TOGGLE_SIDEBAR:
            return !state;
        default:
            return state;
    }
};