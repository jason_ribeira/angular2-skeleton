import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

@Injectable()
export class GlobalActions {
    
    static SET_ROOT_STATE = '[Global] Set Root State';
    
    static setRootState(query? : any) : Action {
        return {
            type: GlobalActions.SET_ROOT_STATE,
            payload: query
        }
    }
    
    static TOGGLE_SIDEBAR = '[Global] Toggle Sidebar';
    
    static toggleSidebar(query? : any) : Action {
        return {
            type: GlobalActions.TOGGLE_SIDEBAR,
            payload: query
        }
    }
    
}