import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

const basicModules = [
    CommonModule,
    FormsModule
];

@NgModule({
    imports: [
        ...basicModules
    ],
    declarations: [],
    exports: [
        ...basicModules
    ],
    providers: [ ]
})
export class SharedModule {
}