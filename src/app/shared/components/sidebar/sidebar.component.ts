import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges } from '@angular/core';

interface subItem {
    title : string;
    route : string;
}

interface menuItem {
    title : string;
    icon : string;
    route : string;
    children : subItem[]
}

@Component({
    selector: 'r2-sidebar',
    template: require('./sidebar.component.html'),
    styles: [ require('./sidebar.component.scss') ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarComponent implements OnInit, OnChanges {
    
    @Input('sidebarState') sidebarState : boolean;
    @Output() openSidebar : EventEmitter<any> = new EventEmitter<any>();
    
    menu : menuItem[] = [
        {
            title: 'Dashboard',
            icon: 'av_timer',
            route: '/',
            children: []
        }
    ];
    
    selectedMenuIndex : number;
    
    constructor() {
    }
    
    ngOnInit() {
    }
    
    ngOnChanges() {
        if (!this.sidebarState) this.selectedMenuIndex = null;
    }
    
    toggleChildren(index) {
        if (index === this.selectedMenuIndex) {
            this.selectedMenuIndex = null;
        } else {
            this.selectedMenuIndex = index;
        }
        
        if (!this.sidebarState) this.openSidebar.emit();
    }
    
    clearOpen() {
        this.selectedMenuIndex = null;
    }
    
    showSubMenu(index) {
        return index === this.selectedMenuIndex;
    }
    
}