import { Directive, ElementRef, ContentChild, Input, OnChanges, OnInit, Renderer } from "@angular/core";

@Directive({
    selector: '[sidebarSubMenu]'
})
export class SidebarSubMenuDirective implements OnInit, OnChanges {
    @Input('sidebarSubMenu') sidebarSubMenu : boolean;
    @ContentChild('subMenu') subMenu : ElementRef;
    
    height : number;
    
    constructor(private el : ElementRef, private renderer : Renderer) {
    }
    
    ngOnInit() {
    }
    
    ngOnChanges() {
        this.sidebarSubMenu ? this.show() : this.hide();
    }
    
    show() {
        this.height = this.subMenu.nativeElement.clientHeight;
        this.renderer.setElementStyle(this.el.nativeElement, 'height', this.height + 'px');
    }
    
    hide() {
        this.renderer.setElementStyle(this.el.nativeElement, 'height', '0px');
    }
    
}