import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'r2-header',
    template: require('./header.component.html'),
    styles: [ require('./header.component.scss') ]
})
export class HeaderComponent implements OnInit {
    @Output() toggleSidebar : EventEmitter<any> = new EventEmitter<any>();
    
    constructor() {
    }
    
    ngOnInit() {
    }
    
}