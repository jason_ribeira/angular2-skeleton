import { Routes, RouterModule } from '@angular/router';

const appRoutes : Routes = [
];

export const appRoutingProviders : any[] = [];

export const baseRouting = RouterModule.forRoot(appRoutes);