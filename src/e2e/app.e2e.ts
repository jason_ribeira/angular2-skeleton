import { AppPage } from './app.page';

describe('App', () => {
    let page: AppPage;

    page = new AppPage();

    page.navigateTo();

    beforeEach(() => {
    });

    it('should have a title', () => {
        let subject = browser.getTitle();
        let result = 'Angular 2 Skeleton';
        expect(subject).toEqual(result);
    });

    it('should have header', () => {
        let subject = element(by.id('mainTitle')).isPresent();
        let result = true;
        expect(subject).toEqual(result);
    });

    it('should say hello world!', () => {
        let content = page.getMainTitleText();
        let result = 'hello world!';
        expect(content).toEqual(result);
    });

});