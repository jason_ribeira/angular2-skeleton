export class AppPage {

    navigateTo() {
        return browser.get('/');
    }
    
    getMainTitleText() {
        return element(by.id('mainTitle')).getText();
    }
}
