import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { bootloader } from '@angularclass/hmr';
import { enableProdMode } from '@angular/core';

import { AppModule } from "./app/app.module";
import { decorateModuleRef } from "./environment";

if (process.env.ENV === 'production') {
    enableProdMode();
}

export function main() {
    //noinspection TypeScriptUnresolvedFunction
    platformBrowserDynamic()
        .bootstrapModule(AppModule)
        .then(decorateModuleRef)
        .catch(err => console.log(err));
}

bootloader(main);